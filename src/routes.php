<?php

$app->get('/robots.txt', function ($request, $response, $args) {
  $response->getBody()->write("User-agent: *\nDisallow: /");
  return $response->withStatus(200)->withHeader('Content-Type', 'text/plain');
});

$app->get('/H', function ($request, $response, $args) {
  $controller = new \Tor\DonateController($this);
  return $controller->index($request, $response, $args);
});

$app->get('/campaign-totals', function($request, $response, $args) {
  $controller = new \Tor\CampaignController($this);
  return $controller->totals($request, $response, $args);
});

$app->get('/civicrm/mailing/{method}', function($request, $response, $args) {
  $controller = new \Tor\CivicrmMailingController($this);
  return $controller->process($request, $response, $args);
});

$app->post('/cryptocurrency/donate', function ($request, $response, $args) {
  $controller = new \Tor\CryptocurrencyController($this);
  return $controller->donate($request, $response, $args);
});

$app->post('/paypal-ipn', function($request, $response, $args) {
  $controller = new \Tor\PaypalIpnController($this);
  return $controller->index($request, $response, $args);
});

$app->get('/pdr', function ($request, $response, $args) {
  return $response->withRedirect("/");
})->setName('home');

$app->get('/process-paypal', function($request, $response, $args) {
  $controller = new \Tor\PaypalController($this);
  return $controller->processFromGet($request, $response, $args);
});

$app->post('/process-paypal', function($request, $response, $args) {
  $controller = new \Tor\PaypalController($this);
  return $controller->processFromPost($request, $response, $args);
})->setName('process-paypal');

$app->post('/process-stripe', function($request, $response, $args) {
  $controller = new \Tor\StripeController($this);
  return $controller->process($request, $response, $args);
});

$app->post('/setExpressCheckout', function($request, $response, $args) {
  $controller = new \Tor\PaypalController($this);
  return $controller->setExpressCheckout($request, $response, $args);
});

$app->post('/stripe-webhook', function($request, $response, $args) {
  $controller = new \Tor\StripeWebhookController($this);
  return $controller->index($request, $response, $args);
});

$app->get('/subscribe', function($request, $response, $args) {
  $controller = new \Tor\SubscriptionController($this);
  return $controller->form($request, $response, $args);
});

$app->get('/subscription-confirm', function($request, $response, $args) {
  $controller = new \Tor\SubscriptionController($this);
  return $controller->subscriptionConfirm($request, $response, $args);
})->setName('subscription-confirm');

$app->post('/subscription-request', function($request, $response, $args) {
  $controller = new \Tor\SubscriptionController($this);
  return $controller->subscriptionRequest($request, $response, $args);
});

$app->get('/subscription-request-sent', function($request, $response, $args) {
  $controller = new \Tor\SubscriptionController($this);
  return $controller->subscriptionRequestSent($request, $response, $args);
});

$app->get('/subscribed', function($request, $response, $args) {
  $controller = new \Tor\SubscriptionController($this);
  return $controller->subscribed($request, $response, $args);
});

$app->get('/thank-you', function($request, $response, $args) {
  $controller = new \Tor\DonateController($this);
  return $controller->thank_you($request, $response, $args);
});

$app->get('/civicrm/redirect', function($request, $response, $args) {
  $controller = new \Tor\MailingRedirects($this);
  return $controller->process($request, $response, $args);
});

$app->get('/captcha', function($request, $response, $args) {
  $controller = new \Tor\Captcha($this);
  return $controller->generate($request, $response, $args);
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});
