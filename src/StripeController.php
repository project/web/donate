<?php

namespace Tor;

class StripeController extends BaseController {
  protected $hmac;
  const CURRENCY_CODE = 'USD';

  function getMonthlyPlanForAmount($amount) {
    $currency = static::CURRENCY_CODE;
    $plan_id = "web_donation_monthly_{$currency}_{$amount}";

    // Try to retrieve a pre-existing plan.
    $plan = NULL;
    try {
      $plan = \Stripe\Plan::retrieve($plan_id);
    } catch (\Stripe\Error\InvalidRequest $e) {
      // Create new plan
      $plan = \Stripe\Plan::create(array(
        "name" => $plan_id,
        "id" => $plan_id,
        "interval" => "month",
        "currency" => $currency,
        "amount" => $amount,
      ));
    }
    return $plan;
  }

  function createCustomer($email, $token) {
    $customer = \Stripe\Customer::create(array(
      'email' => $email,
      'source'  => $token,
    ));
    return $customer;
  }
  function createSubscription($customer_id, $plan_id) {
    $subscription = \Stripe\Subscription::create(array(
      "customer" => $customer_id,
      "plan" => $plan_id,
    ));
    return $subscription;
  }

  function process($request, $response, $args) {
    StripeConfig::setup();
    $parsedBody = $request->getParsedBody();
    $responseData = array(
      'errors' => array(),
    );
    $this->logger->debug('stripe donation request from: ' . $_SERVER['REMOTE_ADDR']);

    # rate limit donation attempts per email and ip addresses
    try {
      $this->container->get('ipRateLimiter')->check($request);
      $this->container->get('emailRateLimiter')->check($request);
    } catch (\Tor\IpRateExceeded | \Tor\EmailRateExceeded $e) {
      $responseData['errors'][] = "Reached maximum number of attempts, please try again later.";
      $this->logger->error($e->getMessage());
      return $response->withJson($responseData);
    }

    $fields = ArrayExt::fetch($parsedBody, "fields");
    $captcha = new \Tor\Captcha();
    $valid = $captcha->is_valid($fields, $_SESSION);
    if (!$valid) {
      $responseData['errors'][] = "Captcha is incorrect, please check your input.";
      $this->logger->error('invalid CAPTCHA from: ' . $_SERVER['REMOTE_ADDR']);
      return $response->withJson($responseData);
    }

    $token = ArrayExt::fetch($parsedBody, 'token');
    if ($token === NULL || trim($token) === '') {
      $responseData['errors'][] = "Missing Stripe token. Please contact frontdesk(at)rt.torproject.org for support.";
      $this->logger->error('missing stripe token from: ' . $_SERVER['REMOTE_ADDR']);
      return $response->withJson($responseData);
    }

    $amount = ArrayExt::fetchRequired($parsedBody, 'amount');
    if (preg_match('/^\d+$/', $amount) !== 1) {
      $responseData['errors'][] = "amount must be an integer";
      $this->logger->error('amount not an integer from: ' . $_SERVER['REMOTE_ADDR']);
      return $response->withJson($responseData);
    }
    $amount = intval($amount);
    if ($amount < 200) {
      $responseData['errors'][] = "\$2 minimum donation";
      $this->logger->error('amount too small from: ' . $_SERVER['REMOTE_ADDR']);
      return $response->withJson($responseData);
    }
    $recurring = ArrayExt::fetch($parsedBody, 'recurring', FALSE);
    $chargeArgs = array(
      'amount' => $amount,
      'currency' => static::CURRENCY_CODE,
      'description' => 'Donation to the Tor Project',
      'source' => $token,
    );

    if ($recurring) {
      try{
        $plan = $this->getMonthlyPlanForAmount($amount);
        $customer = $this->createCustomer($parsedBody['fields']['email'], $token);
        $subscription = $this->createSubscription($customer->id, $plan->id);
        $this->sendDonationInfoToCrm($parsedBody, $subscription->id, $recurring, $amount/100);
      } catch (\Stripe\Error\Card $e) {
        $body = $e->getJsonBody();
        $error = $body['error'];
        $responseData['errors'][] = $error['message'];
      }
    }
    else {
      try {
        $charge = \Stripe\Charge::create($chargeArgs);
        $this->sendDonationInfoToCrm($parsedBody, $charge->id, $recurring, $amount/100);
      } catch (\Stripe\Error\Card $e) {
        $body = $e->getJsonBody();
        $error = $body['error'];
        $responseData['errors'][] = $error['message'];
      }
    }
    if ($body['error'] && $body['error']['type'] == 'card_error') {
      // log failed transactions so fail2ban can ratelimit fraudulent transactions
      $this->logger_ratelimit->error('stripe: failed transaction from ' . $_SERVER['REMOTE_ADDR']);
      $this->logger->error('stripe transaction failure: ' . json_encode($chargeArgs) . ', from: ' . $_SERVER['REMOTE_ADDR'] . ', response: ' . json_encode($responseData));
      return $response->withJson($responseData);
    }
    $this->logger->info('stripe transaction success: ' . json_encode($chargeArgs) . ', from: ' . $_SERVER['REMOTE_ADDR'] . ', response: ' . json_encode($responseData));
    return $response->withJson($responseData);
  }

  function sendDonationInfoToCrm($parsedBody, $trxn_id, $recurring, $amount) {
    $fieldHelper = new FieldHelper();
    $donationInfo = $fieldHelper->createDonationInfo($parsedBody);
    $donationInfo['payment_instrument_id'] = 'Credit Card';
    $donationInfo['currency'] = static::CURRENCY_CODE;
    $now = new \DateTime('now', new \DateTimeZone('UTC'));
    $donationInfo['receive_date'] = $now->format('Y-m-d\TH:i:s\Z');
    $donationInfo['total_amount'] = $amount;
    $donationInfo['trxn_id'] = $trxn_id;
    $crmController = new CrmController($this->container);
    if ($recurring) {
      $crmController->sendMessage('Tor\Donation\RecurringContributionSetup', $donationInfo);
    } else {
      $crmController->sendMessage('Tor\Donation\OneTimeContribution', $donationInfo);
    }
  }
}
