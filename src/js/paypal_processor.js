/*
 * All of this file is to work around the PayPal first party isolation problem.
 * See the README for details.
 */
var $ = require('jquery');

function PaypalProcessor(token, payerId) {
  var data = localStorage.getItem('donationData');
  if (data === null) {
    this.showPaymentErrors(["Couldn't find donationData in local storage."]);
  } else {
    data = JSON.parse(data);
    data['PayerID'] = payerId;
    data['token'] = token;
    var ajaxSettings = {
      contentType: 'application/json; charset=UTF-8',
      data: JSON.stringify(data),
      dataType: 'json',
      type: 'POST',
      url: '/process-paypal',
    };
    $.ajax(ajaxSettings)
      .done($.proxy(this.paypalProcessDone, this))
      .fail($.proxy(this.paypalProcessFailed, this));
  }
}

PaypalProcessor.prototype.paypalProcessDone = function(data, textStatus, jqXHR) {
  if (data['errors'].length > 0) {
    this.showPaymentErrors(data['errors']);
  } else {
    localStorage.setItem('donationData', null);
    localStorage.setItem('donationDone', true);
    window.location.href = "/thank-you";
  }
}

PaypalProcessor.prototype.paypalProcessFailed = function(jqXHR, textStatus, errorThrown) {
  var errorMessage = "Error processing payment:";
  errorMessage = errorMessage + " " + textStatus + ": " + errorThrown;
  this.closeLoading();
  this.showPaymentErrors([errorMessage]);
}

PaypalProcessor.prototype.showPaymentErrors = function(errors) {
  $('.errors-area').show();
  $('.title').html("Errors processing donation");
  var errorsContainer = $('.errors');
  $.each(errors, function(index, error) {
    var message = '';
    if (typeof error === 'string' || error instanceof String) {
      message = error;
    }
    else if ("message" in error) {
      message = error['message'];
    }
    errorsContainer.append('<li>' + message + '</li>');
  });
}

module.exports = PaypalProcessor;
