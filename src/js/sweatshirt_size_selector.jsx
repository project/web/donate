import React from 'react';

export function SweatshirtSizeSelector(props) {
  const { sweatshirtSizes, updateFitsAndSizes, fitsAndSizes } = props;

  const sweatShirtSizeChange = (event) => {
    updateFitsAndSizes('sweatshirt', null, event.target.value);
  }

  return (
    <React.Fragment>
      <React.Fragment>
        <div id="selected-perk-fields-label">Sweatshirt</div>
          <div className="fit-options-div">
            <select name="sweatshirt-size" className="field input size required" onChange={sweatShirtSizeChange}>
              <option value={null}>Select Size</option>
              {sweatshirtSizes.map(id =>
                <option key={id} value={id}>{id.toUpperCase()}</option>
              )}
            </select>
          </div>
      </React.Fragment>
    </React.Fragment>
  );
}
