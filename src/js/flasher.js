var $ = require('jquery');

function Flasher(target) {
  this.target = $(target);
  this.build();
  this.state = 'flashing';
}

Flasher.prototype.build = function() {
  this.characters = this.target.find('.character');
  this.numCharacters = this.characters.length;
  setTimeout($.proxy(this.flash, this), 100);
}

Flasher.prototype.flash = function() {
  this.characters.each(function() {
    $(this).removeClass('covered');
    if (Math.random() <= 0.5) {
      $(this).addClass('covered');
    }
  });
  if (this.state != 'resolved') {
    setTimeout($.proxy(this.flash, this), 100);
  }
}

Flasher.prototype.startCharacterResolve = function() {
  var index = this.numCharacters - this.numResolvedCharacters - 1;
  this.characterToResolve = $(this.characters[index]);
  this.characterToResolve.addClass('covered');
  this.characters = this.characters.slice(0, index);
  setTimeout($.proxy(this.resolveCharacter, this), 100);
}

Flasher.prototype.resolve = function(value) {
  if (this.state == 'resolved') {
    this.update(value);
  } else {
    this.resolvedCharacters = value;
    this.numResolvedCharacters = 0;
    this.delayCount = 0;
    this.state = 'resolving';
    setTimeout($.proxy(this.startCharacterResolve, this), 500);
  }
}

Flasher.prototype.resolveCharacter = function() {
  this.characterToResolve.removeClass('covered');
  var resolvedIndex = this.resolvedCharacters.length - this.numResolvedCharacters - 1;
  if (resolvedIndex >= 0) {
    this.characterToResolve.addClass('resolved');
    this.characterToResolve.html(this.resolvedCharacters[resolvedIndex]);
  }
  this.numResolvedCharacters += 1;
  if (this.numResolvedCharacters == this.numCharacters) {
    this.characters.removeClass('covered');
    this.state = 'resolved';
    return;
  }
  setTimeout($.proxy(this.startCharacterResolve, this), 500);
}

Flasher.prototype.update = function(value) {
  this.characters = this.target.find('.character');
  for (var i = 0; i < this.numCharacters; ++i) {
    var characterIndex = this.numCharacters - i - 1;
    var valueIndex = value.length - i - 1;
    var character = $(this.characters[characterIndex]);
    if (valueIndex >= 0) {
      character.html(value[valueIndex]);
    }
  }
}

module.exports = Flasher;
