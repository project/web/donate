export function isFloat(value) {
  const matcher = /^\d*\.?\d*$/
  return matcher.test(value);
}

export function showCommaForThousands(integerNumber) {
  return integerNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
