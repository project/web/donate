import React, {useState} from 'react';
import {NamedError} from './named_error';
import {CardNumberElement, CardExpiryElement, CardCVCElement} from 'react-stripe-elements';

const createOptions = (placeholder, fieldName) => {
  return {
    placeholder: placeholder,
    classes: {
      base: `field ${fieldName}`,
      invalid: 'field error',
    },
    style: {
      base: {
        fontSize: '16px',
        color: '#484848',
        letterSpacing: '0.025em',
        '::placeholder': {
          fontFamily: '"Source Sans Pro", sans-serif'
        },
        fontFamily: '"Source Sans Pro", sans-serif'
      },
      invalid: {
        color: 'red',
      },
    },
  };
};

export function StripeCreditCardForm(props) {
  const {onStripeFieldChange} = props;

  return (
    <React.Fragment>
      <div className="split-form stripe-elements">
        <div className="field-row">
          <CardNumberElement
            {...createOptions('Card Number', 'card-number')}
            onChange={onStripeFieldChange}
          />
        <img className='credit-cards' src='/images/credit-cards.png'/>
        </div>
        <div className="field-row">
          <CardExpiryElement
            {...createOptions('MM/YY', 'exp-date')}
            onChange={onStripeFieldChange}
          />
          <CardCVCElement
            {...createOptions('CVC', 'cvc')}
            onChange={onStripeFieldChange}
          />
        </div>
      </div>
    </React.Fragment>
  );
}
