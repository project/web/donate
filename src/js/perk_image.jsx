import React from 'react';

export function PerkImage(props) {
  const {perk, perkOption} = props;

  let imageSource = perk.image;
  if (perk.options !== null) {
    for (const option of perk.options) {
      if (option.name == perkOption) {
        imageSource = option.image;
      }
    }
  }

  return (
    <img name={perk.name} src={imageSource} width={perk.img_width} height={perk.img_height} />
  );
}
