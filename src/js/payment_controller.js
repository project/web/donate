var countries = require('./countries.js');
var Flasher = require('./flasher');
var regions = require('./regions.js');
var roundTo = require('./round-to');
var numeral = require('numeral');
var $ = require('jquery');
var I18n = require('./i18n.js');
var GiftMatchingController = require('./gift_matching_controller');

var t = I18n.t;

function PerkInfo(id, fieldIds, helpText, friendlyName, friendlyName2) {
  this.id = id;
  this.fieldIds = fieldIds;
  this.helpText = helpText;
  this.friendlyName = friendlyName;
  this.friendlyName2 = friendlyName2;
}

function PaymentController(paypalMerchantId, environmentName, stripePublishableKey, promo = false) {
  this.country = 'US';
  this.priceSetName = 'once';
  this.promo = promo;
  this.setDefaultOnceAmount();
  this.environmentName = environmentName;
  this.noPerk = true;
  this.noRegions = false;
  this.perk = 't-shirt-pack';
  this.paypalMerchantId = paypalMerchantId;
  this.paymentMethod = null;
  this.paymentMethodName = 'credit_card';
  this.paymentMethodToSelector = {
    'credit_card': '.credit-card-form-wrapper,#donate-submit-button',
    'paypal': '#paypal-container,#paypal-button-area',
  };
  this.perkInfoMap = {
    'stickers': new PerkInfo('stickers', [], null, t('t-sticker__friendly-name')),
    't-shirt': new PerkInfo('t-shirt', ['perk-style-1', 'perk-fit-1', 'perk-size-1'], t('t-t-shirt__help-text'), t('t-t-shirt__friendly-name')),
    't-shirt-pack': new PerkInfo('t-shirt-pack', ['perk-style-1', 'perk-fit-1', 'perk-size-1', 'perk-fit-2', 'perk-size-2'], t('t-t-shirt-pack__help-text'), t('t-t-shirt-pack-1__friendly-name'), t('t-t-shirt-pack-2__friendly-name')),
    'sweatshirt': new PerkInfo('sweatshirt', ['perk-size-1'], t('t-sweatshirt__help-text'), t('t-sweatshirt__friendly-name')),
  };
  this.tShirtStyleOptions = {
    'take-back-internet': {
      'slim': {
        'friendly-name': t('t-t-shirt-style__slim'),
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': t('t-t-shirt-style__classic'),
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
    },
    'strength-in-numbers': {
      'slim': {
        'friendly-name':  t('t-t-shirt-style__slim'),
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': t('t-t-shirt-style__classic'),
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
    },
  };
  this.priceSets = {
    'once' : [
      [2500, 'stickers'],
      [7500, 't-shirt'],
      [12500, 't-shirt-pack'],
      [25000, 't-shirt-pack'],
      [50000, 'sweatshirt'],
    ],
    'monthly': [
      [500, undefined],
      [1000, 'stickers'],
      [2500, 't-shirt'],
      [5000, 't-shirt-pack'],
      [10000, 'sweatshirt'],
    ],
  };
  this.perkRequiredFieldIds = {
    'credit_card': [
      'country',
      'cardNumber',
      'cvc',
      'email',
      'expMonth',
      'expYear',
      'firstName',
      'lastName',
      'locality',
      'postalCode',
      'region',
      'streetAddress',
      'captcha'
    ],
    'paypal': [
      'country',
      'email',
      'firstName',
      'lastName',
      'locality',
      'postalCode',
      'region',
      'streetAddress',
      'captcha'
    ],
  };
  this.standardPaymentRequiredFieldIds = {
    'paypal': [
      'email',
      'firstName',
      'lastName',
      'captcha',
    ],
    'credit_card': [
      'country',
      'cardNumber',
      'cvc',
      'email',
      'expMonth',
      'expYear',
      'firstName',
      'lastName',
      'locality',
      'postalCode',
      'region',
      'streetAddress',
      'captcha',
    ]
  };
  this.stripePublishableKey = stripePublishableKey;
  this.init();
}

PaymentController.prototype.init = function() {
  this.setInitialPromoAdjustments();
  this.setPriceSet();
  this.setRecurring();
  this.setCurrentPrice();
  this.setPaymentMethodName();
  this.setupRecurringButton();
  this.setupPayPal();
  this.setupPriceButtons();
  this.setupPerkButtons();
  this.setupPaymentMethodButtons();
  this.setupStripe();
  this.setupCountries();
  this.setCountry();
  this.setupPerkFields();
  this.setupPerkSizes();
  this.setupResizePlaceholdertext();
}

PaymentController.prototype.setupResizePlaceholdertext = function() {
  var self = this;
  // Initial resize of each input placeholder if needed.
  $('input[type=text]').each(function() {
    self.resizePlaceholdertextForElement(this);
  });
  // Resize each placeholder if it re-appears and/or
  // resize input field for user inputted text.
  $('body').on('change keyup', 'input[type=text]', function(e) {
    self.resizePlaceholdertextForElement(e.target);
  });
}

PaymentController.prototype.resizePlaceholdertextForElement = function(el) {
  // Step is used to reduce font-size by value X
  var step = 0.2;

  // Only change font-size if input value is empty (font-size should only affect placeholder)
  if ($(el).val() === '') {
    // Create clone
    $clone = $('<span></span>')
      .appendTo('body')
      .addClass('inputClone')
      .css('font-weight', $(el).css('font-weight'))
      .css('text-transform', $(el).css('text-transform'))
      .text($(el).attr('placeholder'));
    // Adjust font-size of clone
    var fontSize = $(el).css('font-size');
    while ($clone.width() > $(el).width()) {
      fontSize = parseFloat($clone.css('font-size')) - step;
      $clone.css({
        'font-size': fontSize
      });
    }
    // Maintain input field size
    $(el).css({
      "width": $(el).width() + "px",
      "height": $(el).height() + "px"
    });
    // Change input font-size
    $(el).animate({
      'font-size': fontSize
    });
    // Delete clone
    $clone.remove();
  } else {
    // Default input field back to normal
    $(el)
      .finish()
      .clearQueue()
      .attr('style', function(i, style) {
        // Remove inline style for font-size
        if (style) {
          return style.replace(/font-size[^;]+;?/g, '');
        }
      });
  }
}

PaymentController.prototype.clearActivePerk = function() {
  $('.perk').removeClass('selected');
}

PaymentController.prototype.clearErrors = function() {
  $('input,div').removeClass('error');
  $('.error-container').remove();
}

PaymentController.prototype.clearOtherAmount = function() {
  $('#otherAmount').val('');
}

PaymentController.prototype.closeLoading = function() {
  tor.reactCallbacks.setLoadingDialogOpen(false);
}

PaymentController.prototype.countryChanged = function(event) {
  this.setupRegions();
}

PaymentController.prototype.displayCurrentPerk = function() {
  this.clearActivePerk();
  if (!this.noPerk) {
    var selectedPerk = $(".perk[data-perk='" + this.perk + "']");
    selectedPerk.addClass('selected');
  }
  this.displaySelectedPerkInSubmitArea();
}

PaymentController.prototype.displaySelectedPerkInSubmitArea = function() {
  var submitAreaPerkSelected = $('#donate-submit-perk');
  if (this.noPerk) {
    submitAreaPerkSelected.html(t('t-no-gift'));
  } else {
    var displayedPerk = t('t-gift-selected-colon') + ' ';
    if (this.perk == 't-shirt-pack') {
      displayedPerk += t('t-t-shirt-pack__friendly-name');
    } else if (this.perkInfoMap.hasOwnProperty(this.perk)) {
      displayedPerk += this.perkInfoMap[this.perk].friendlyName;
    }
    submitAreaPerkSelected.html(displayedPerk);
  }
}

PaymentController.prototype.getCampaignTotalsDone = function(data, textStatus, jqXHR) {
  if (data['errors'].length > 0) {
    console.log("Error fetching campaign totals from /campaign-totals", data);
  } else {
    this.setCampaignTotals(data['data']);
  }
  window.setTimeout($.proxy(this.requestCampaignTotals, this), 5000);
}

PaymentController.prototype.getCampaignTotalsFailed = function(jqXHR, textStatus, errorThrown) {
  console.log("Error fetching campaign totals from /campaign-totals", textStatus, errorThrown);
}

PaymentController.prototype.getDonationData = function() {
  var fieldValues = $('#donationForm').serializeArray();
  var perkName = this.perk;
  if (this.noPerk) {
    perkName = 'none';
  }
  var perkValues = {
    'name': perkName,
  };
  if (!this.noPerk && this.perkInfoMap.hasOwnProperty(this.perk)) {
    var perkInfo = this.perkInfoMap[this.perk];
    $.each(perkInfo.fieldIds, function(index, perkFieldId) {
      valueName = perkFieldId.replace(/^perk-/, '');
      perkValues[valueName] = $('#' + perkFieldId).val();
    });
  }
  var data = {
    'amount': this.amount,
    'fields': {},
    'paymentMethod': {
      'name': this.paymentMethodName,
    },
    'recurring': this.recurring(),
  };
  if (perkName !== undefined) {
    data['perk'] = perkValues;
  }
  $.each(fieldValues, function(index, field) {
    var value = field['value'].trim();
    if (field['name'] == 'region' && value == '--none--') {
      value = '';
    }
    if (value != '') {
      data['fields'][field['name']] = value;
    }
  });
  return data;
}

PaymentController.prototype.getPerkElements = function() {
  var perkElements = $('#notmatchinganything');
  if (!this.noPerk && this.perkInfoMap.hasOwnProperty(this.perk)) {
    var perkInfo = this.perkInfoMap[this.perk];
    $.each(perkInfo.fieldIds, function(index, perkFieldId) {
      perkElements = perkElements.add($('#'  + perkFieldId));
    });
  }
  return perkElements;
}

PaymentController.prototype.MissingRequiredFieldException = function(message) {
   this.message = message;
   this.name = 'MissingRequiredFieldException';
}

PaymentController.prototype.getRequiredFields = function() {
  var requiredFields = $('#notmatchanything');
  $.each(this.standardPaymentRequiredFieldIds[this.paymentMethodName], function(index, fieldId) {
    if ($('#' + fieldId).length > 0) {
      requiredFields = requiredFields.add('#' + fieldId);
    }
    else {
      // A required form element is missing.
      throw new PaymentController.prototype.MissingRequiredFieldException(t('t-missing-required-field-exception'));
    }
  });
  if (this.perk != null && !this.noPerk) {
    $.each(this.perkRequiredFieldIds[this.paymentMethodName], function(index, fieldId) {
      requiredFields = requiredFields.add('#' + fieldId);
    });
  }
  requiredFields = requiredFields.add(this.getPerkElements());
  if (this.noRegions) {
    requiredFields = requiredFields.not('#region');
  }
  $('#generic-validation-messages').empty();
  return requiredFields;
}

PaymentController.prototype.monthlyButtonClicked = function(event) {
  event.preventDefault();
  this.clearOtherAmount();
  this.priceSetName = 'monthly';
  this.amount = 2500;
  this.perk = 't-shirt';
  this.setRecurring();
  this.setPriceSet();
  this.setCurrentPrice();
  this.setCurrentPerk();
  this.setLimitedOfferLabelOnPriceLabel();
}

PaymentController.prototype.noPerkCheckboxClicked = function(event) {
  if ($(event.target).prop('checked')) {
    this.noPerk = true;
  } else {
    this.noPerk = false;
  }
  this.setCurrentPerk();
}

PaymentController.prototype.onceButtonClicked = function(event) {
  event.preventDefault();
  this.clearOtherAmount();
  this.setDefaultOnceAmount();
  this.perk = 't-shirt-pack';
  this.priceSetName = 'once';
  this.setPriceSet();
  this.setCurrentPrice();
  this.setRecurring();
  this.setCurrentPerk();
  this.setLimitedOfferLabelOnPriceLabel();
}

PaymentController.prototype.onFormSubmit = function(event) {
  event.preventDefault();
  this.clearErrors();
  var errors = this.validateRequiredFields();
  if (errors.length > 0) {
    this.showPaymentError(errors);
    return false;
  }
  this.showLoading();
  var form = $('#donationForm');
  form.find('.submit').prop('disabled', true);
  Stripe.card.createToken(form, $.proxy(this.stripeResponseHandler, this));
  return false;
}

PaymentController.prototype.otherAmountChanged = function(event) {
  var priceString = $('#otherAmount').val().trim();
  var priceFloat = 0;

  if (priceString != '') {
    priceFloat = parseFloat(priceString);
    if (isNaN(priceFloat)) {
      $(".invalid-amount").show();
      return;
    }
    this.amount = priceFloat * 100;
  }
  $(".invalid-amount").hide();

  // Enforce minimum donation amount of $2
  var otherAmountFields = $('#otherAmount');
  if (this.amount < 200) {
    $(".donate-amount-min").show();
    otherAmountFields.addClass('error');
  }
  else {
    $(".donate-amount-min").hide();
    otherAmountFields.removeClass('error');
    this.perk = undefined;
    var priceLevels = this.priceSets[this.priceSetName];
    $.each(priceLevels, $.proxy(function(index, levelInfo) {
      if (this.amount >= levelInfo[0]) {
        this.perk = levelInfo[1];
      }
    }, this));
    this.setCurrentPrice();
  }
}

PaymentController.prototype.paymentMethodButtonClicked = function(event) {
  event.preventDefault();
  var paymentMethodElement = $(event.target).closest('[data-payment-method]');
  this.paymentMethodName = paymentMethodElement.data('payment-method');
  this.setPaymentMethodName();
}

PaymentController.prototype.paypalCheckDonationDone = function() {
  var data = localStorage.getItem('donationDone');
  if (data == 'true') {
    localStorage.setItem('donationDone', null);
    window.location.href = "/thank-you";
  } else {
    this.paypalProblemTimeout = setTimeout($.proxy(this.paypalCheckDonationDone, this), 1000);
  }
}

PaymentController.prototype.paypalOnAuthorize = function(paypalResponse, actions) {
  this.showLoading();
  data = this.getDonationData();
  data['PayerID'] = paypalResponse.payerID;
  data['token'] = paypalResponse.paymentToken;
  var ajaxSettings = {
    contentType: 'application/json; charset=UTF-8',
    data: JSON.stringify(data),
    dataType: 'json',
    type: 'POST',
    url: '/process-paypal',
  };
  $.ajax(ajaxSettings)
    .done($.proxy(this.paypalProcessDone, this))
    .fail($.proxy(this.paypalProcessFailed, this));
}

PaymentController.prototype.paypalOnCancel = function(data, actions) {
  this.closeLoading();
}

PaymentController.prototype.paypalProcessDone = function(data, textStatus, jqXHR) {
  if (data['errors'].length > 0) {
    this.closeLoading();
    this.showPaymentError(data['errors']);
  } else {
    window.location.href = "/thank-you";
  }
}

PaymentController.prototype.paypalProcessFailed = function(jqXHR, textStatus, errorThrown) {
  var errorMessage = t('t-payment-processor-failed');
  errorMessage = errorMessage + " " + textStatus + ": " + errorThrown;
  this.closeLoading();
  this.showPaymentError([errorMessage]);
}

PaymentController.prototype.paypalSetExpressCheckout = function(resolve, reject) {
  var errors = this.validateRequiredFields();
  if (errors.length > 0) {
    this.showPaymentError(errors);
    reject(t('t-validation-failed'));
  }
  data = this.getDonationData();
  localStorage.setItem('donationData', JSON.stringify(data));
  var ajaxSettings = {
    contentType: 'application/json; charset=UTF-8',
    data: JSON.stringify(data),
    dataType: 'json',
    type: 'POST',
    url: '/setExpressCheckout',
  };
  $.ajax(ajaxSettings)
    .done($.proxy(this.paypalSetExpressCheckoutDone, this, resolve, reject))
    .fail($.proxy(this.paypalSetExpressCheckoutFailed, this, resolve, reject));
}

PaymentController.prototype.paypalSetExpressCheckoutDone = function(resolve, reject, data, textStatus, jqXHR) {
  if (data['errors'].length > 0) {
    this.closeLoading();
    this.showPaymentError(data['errors']);
    reject(data['errors'].join("\n"));
  } else {
    resolve(data['token']);
  }
}

PaymentController.prototype.paypalSetExpressCheckoutFailed = function(resolve, reject, jqXHR, textStatus, errorThrown) {
  var errorMessage = t('t-payment-processor-failed')  + textStatus + ": " + errorThrown;
  this.showPaymentError([errorMessage]);
  reject(errorMessage);
}

PaymentController.prototype.perkButtonClicked = function(event) {
  var perkElement = $(event.target).closest('[' + this.priceAttrName() + ']');
  if (perkElement.hasClass('disabled')) {
    return;
  }
  this.perk = perkElement.attr('data-perk');
  this.noPerk = false;
  var oldAmount = this.amount;
  var price = parseInt(perkElement.attr(this.priceAttrName()));
  if (price > this.amount) {
    this.amount = price;
    this.clearOtherAmount();
  }
  this.setCurrentPrice();
  if (this.perk == 't-shirt') {
    this.setPerkFields();
  }
}

PaymentController.prototype.perkSubSelectFieldUpdated = function(event) {
  var target = $(event.target);
  var selectedValue = target.val();
  var images = $(target).parents('.perk').find('.perk-img');
  images.hide();
  var selectedImage = images.filter('[data-perk-image=' + selectedValue + ']');
  selectedImage.show();
  this.setPerkFields();
  this.displaySelectedPerkInSubmitArea();
}

PaymentController.prototype.perkFitUpdated = function(event) {
  var target = $(event.target);
  var fit = target.val();
  var perkIndex = 1;
  var style = 'take-back-internet';
  if (this.perk == 't-shirt-pack') {
    style = 'strength-in-numbers';
    if (target.attr('id') === 'perk-fit-2') {
      perkIndex = 2;
      style = 'take-back-internet';
    }
  }

  var availableSizes = this.tShirtStyleOptions[style][fit]['sizes'];
  var perkSizeElement = '#perk-size-' + perkIndex;
  $(perkSizeElement).html('');
  $(perkSizeElement).append($('<option>', {value: '--none--', text: t('t-t-shirt-select-size')}));
  availableSizes.forEach(function(size){
    $(perkSizeElement).append($('<option>', {value: size, text: size.toUpperCase()}));
  });
}

PaymentController.prototype.priceButtonClicked = function(event) {
  event.preventDefault();
  var priceElement = $(event.target);
  var priceString = priceElement.attr('data-price-in-cents');
  this.clearOtherAmount();
  this.amount = parseInt(priceString);
  this.perk = priceElement.attr('data-perk');
  this.setCurrentPrice();
}

PaymentController.prototype.priceAttrName = function() {
  return 'data-' + this.priceSetName + '-price-in-cents';
}

PaymentController.prototype.recurring = function() {
  if (this.priceSetName == 'monthly') {
    return true;
  } else {
    return false;
  }
}

PaymentController.prototype.setCountry = function() {
  $('#country').val(this.country);
  this.setupRegions();
}

PaymentController.prototype.setCurrentPrice = function() {
  $('.price-btn').removeClass('selected');
  var priceLevels = this.priceSets[this.priceSetName];
  $.each(priceLevels, $.proxy(function(index, levelInfo) {
    if (levelInfo[0] == this.amount) {
      $('[data-price-in-cents=' + this.amount + ']').addClass('selected');
      return;
    }
  }, this));
  var perks = $('.perk');
  for (var i = 0; i < perks.length; i++) {
    var perk = $(perks[i]);
    var perkPrice = parseInt(perk.attr(this.priceAttrName()));
    if (perkPrice > this.amount) {
      perk.addClass('disabled');
    } else {
      perk.removeClass('disabled');
    }
  }
  this.updateDonateButtonText();
  this.setCurrentPerk();
}

PaymentController.prototype.setCurrentPerk = function() {
  $('.perk').removeClass('selected');
  if (this.noPerk) {
    this.clearActivePerk();
  } else {
    $('#no-perk-checkbox').prop('checked', false);
  }
  this.displayCurrentPerk();
  this.setPerkFields();
  this.setRequiredFields();
}

PaymentController.prototype.setPaymentMethodName = function() {
  $('.payment-method').removeClass('selected');
  var selectedButton = $("[data-payment-method='" + this.paymentMethodName + "']");
  selectedButton.addClass('selected');
  $.each(this.paymentMethodToSelector, function(key, value) {
    $(value).hide();
  });
  var selector = this.paymentMethodToSelector[this.paymentMethodName];
  $(selector).show();
  this.setRequiredFields();
}

PaymentController.prototype.setPerkFields = function() {
  $('#perk-fields').hide();
  if (!this.noPerk && this.perkInfoMap.hasOwnProperty(this.perk)) {
    $('#perk-fields').show();
    $('#perk-fields select').hide();
    var perkInfo = this.perkInfoMap[this.perk];
    var subSelect = $('.perk[data-perk=' + perkInfo.id + ']').find('.perk-sub-select');
    if (subSelect.length > 0 || this.perk == 't-shirt' || this.perk == 't-shirt-pack') {
      if (this.perk === 't-shirt') {
        $('#selected-perk-fields-label').html(perkInfo.friendlyName + ": " + t('t-t-shirt-pack-2__friendly-name'));
        var tShirtStyleName = 'take-back-internet';
      }
      if (this.perk == 't-shirt-pack') {
        $('#selected-perk-fields-label').html(perkInfo.friendlyName);
        var tShirtStyleName = 'strength-in-numbers';
      }
      var options = this.tShirtStyleOptions[tShirtStyleName];
      $('#perk-fit-1').html('');
      $('#perk-fit-1').append($('<option>', {value: '--none--', text: t('t-t-shirt-select-fit')}));
      $.each(options, function (i, option) {
        $('#perk-fit-1').append($('<option>', {value: i, text: option['friendly-name']}));
        $('#perk-fit-1 option').attr('perk-style', tShirtStyleName);
      });
      $('#perk-style-1').val(tShirtStyleName);
    } else {
      $('#selected-perk-fields-label').html(perkInfo.friendlyName);
    }
    $('#selected-perk-instructions').html(perkInfo.helpText);
    if (perkInfo.friendlyName2 == undefined) {
      $('#selected-perk-fields-label-2').hide();
    } else {
      $('#selected-perk-fields-label-2').html(perkInfo.friendlyName2);
      $('#selected-perk-fields-label-2').show();
    }
    $.each(perkInfo.fieldIds, function(index, perkFieldId) {
      var perkElement = $('#'  + perkFieldId);
      perkElement.show();
    });
  }
}

PaymentController.prototype.setPriceSet = function() {
  var priceButtons = $('.price-btn');
  var priceSet = this.priceSets[this.priceSetName];
  $.each(priceButtons, function(i, priceButton) {
    var priceLevel = priceSet[i];
    if (priceLevel == undefined) {
      $(priceButton).hide();
      return;
    } else {
      $(priceButton).show();
    }
    $(priceButton).attr('data-price-in-cents', priceLevel[0]);
    $(priceButton).html('$' + numeral(priceLevel[0] / 100).format('0'));
    if (priceLevel[1] == undefined) {
      $(priceButton).removeAttr('data-perk');
    } else {
      $(priceButton).attr('data-perk', priceLevel[1]);
    }
  });
}

PaymentController.prototype.setInitialPromoAdjustments = function() {
  this.adjustPriceSetDueToPromo();
}

PaymentController.prototype.setDefaultOnceAmount = function() {
  if (this.promo) {
    this.amount = 6000;
  } else {
    this.amount = 12500;
  }
}

PaymentController.prototype.adjustPriceSetDueToPromo = function() {
  if (this.promo) {
    this.priceSets['once'] = [
      [2500, 'stickers'],
      [6000, 't-shirt'],
      [12500, 't-shirt-pack'],
      [25000, 't-shirt-pack'],
      [50000, 'sweatshirt'],
    ];
  }
}

PaymentController.prototype.setRequiredFields = function() {
  $('.field').removeClass('required');
  var requiredFields = this.getRequiredFields();
  requiredFields.addClass('required');
}

PaymentController.prototype.setRecurring = function() {
  var activeButton = null;
  var inactiveButton = null;
  if (this.recurring()) {
    activeButton = $('#donate-monthly-button');
    inactiveButton = $('#donate-once-button');
  } else {
    activeButton = $('#donate-once-button');
    inactiveButton = $('#donate-monthly-button');
  }
  activeButton.addClass('selected');
  inactiveButton.removeClass('selected');
}

PaymentController.prototype.setLimitedOfferLabelOnPriceLabel = function() {
  if (this.recurring()) {
    $('a[data-perk="t-shirt"]').removeClass('promo');
  } else {
    if (this.promo) {
      $('a[data-perk="t-shirt"]').addClass('promo');
    }
  }
}

PaymentController.prototype.setupCountries = function() {
  var countrySelect = $('#country');
  countrySelect.html('');
  $.each(countries, function(index, country) {
    var option = $('<option>');
    option.attr('value', country[0]);
    option.text(country[1]);
    countrySelect.append(option);
  });
  countrySelect.on('change', $.proxy(this.countryChanged, this));
}

PaymentController.prototype.setupPayPal = function() {
  var self = this;
  var options = {
    payment: function(resolve, reject) {
      self.paypalSetExpressCheckout(resolve, reject);
    },
    onAuthorize: $.proxy(this.paypalOnAuthorize, this),
    onCancel: $.proxy(this.paypalOnCancel, this),
    style: {
      size: 'medium',
      color: 'blue',
    }
  };
  if (this.environmentName == 'prod') {
    options['env'] = 'production';
  } else {
    options['env'] = 'sandbox';
  }
  paypal.Button.render(options, '#paypal-button-area');
  this.paypalProblemTimeout = setTimeout($.proxy(this.paypalCheckDonationDone, this), 1000);
}

PaymentController.prototype.setupPaymentMethodButtons = function() {
  $('.payment-method').on('click', $.proxy(this.paymentMethodButtonClicked, this));
}

PaymentController.prototype.setupPerkButtons = function() {
  $('.perk').on('click', $.proxy(this.perkButtonClicked, this));
  $('#no-perk-checkbox').on('click', $.proxy(this.noPerkCheckboxClicked, this));
}

PaymentController.prototype.setupPerkFields = function() {
  $('.perk-sub-select').on('change', $.proxy(this.perkSubSelectFieldUpdated, this));
}

PaymentController.prototype.setupPerkSizes = function() {
  $('.fit').on('change', $.proxy(this.perkFitUpdated, this));
}

PaymentController.prototype.setupPriceButtons = function() {
  $(".perk-desc.donate-amount-min").hide();
  $('.price-btn').on('click', $.proxy(this.priceButtonClicked, this));
  $('#otherAmount').on('change keyup paste', $.proxy(this.otherAmountChanged, this));
}

PaymentController.prototype.setupRecurringButton = function() {
  var onceButton = $('#donate-once-button');
  onceButton.on('click', $.proxy(this.onceButtonClicked, this));
  var monthlyButton = $('#donate-monthly-button');
  monthlyButton.on('click', $.proxy(this.monthlyButtonClicked, this));
}

PaymentController.prototype.setupRegions = function() {
  var countryCode = $('#country').val();
  var regionNames = regions[countryCode];
  if (regionNames === undefined) {
    $('#region').hide();
    this.noRegions = true;
    return;
  }
  this.noRegions = false;
  $('#region').show();
  var regionSelect = $('#region');
  regionSelect.html('');
  var option = $('<option>');
  option.attr('value', '--none--');
  option.text(t('t-address__state-province-region'));
  regionSelect.append(option);
  $.each(regionNames, function(index, regionName) {
    var option = $('<option>');
    option.attr('value', regionName);
    option.text(regionName);
    regionSelect.append(option);
  });
}

PaymentController.prototype.setupStripe = function() {
  Stripe.setPublishableKey(this.stripePublishableKey);
  var form = $('#donationForm');
  form.submit($.proxy(this.onFormSubmit, this));
}

PaymentController.prototype.showLoading = function() {
  tor.reactCallbacks.setLoadingDialogOpen(true);
}

PaymentController.prototype.showPaymentError = function(errors) {
  var html = '<div class="error-container"><div class="title">error</div>';
  $.each(errors, function(index, error) {
    var message = '';
    if (typeof error === 'string' || error instanceof String) {
      message = error;
    }
    else if ("message" in error) {
      message = error['message'];
    }
    html += '<p class="error perk-desc">' + message + '</p>';
  });
  html += '</div>';
  $('.info-area').after(html);
}

PaymentController.prototype.sliderCurrentPage = function() {
  var page = 1;
  var sliderButton = $('#perk-slider .w-slider-dot:first');
  if (sliderButton.length > 0 && !sliderButton.hasClass('w-active')) {
    page = 2;
  }
  return page;
}

PaymentController.prototype.stripeResponseHandler = function(status, response) {
  if (response.error) {
    this.closeLoading();
    this.showPaymentError([response.error.message]);
    var form = $('#donationForm');
    form.find('.submit').prop('disabled', false);
  } else {
    this.processStripe(response);
  }
}

PaymentController.prototype.processStripe = function(stripeResponse) {
  this.showLoading();
  data = this.getDonationData();
  data['token'] = stripeResponse.id;
  var ajaxSettings = {
    contentType: 'application/json; charset=UTF-8',
    data: JSON.stringify(data),
    dataType: 'json',
    type: 'POST',
    url: '/process-stripe',
  };
  $.ajax(ajaxSettings)
    .done($.proxy(this.processStripeDone, this))
    .fail($.proxy(this.processStripeFailed, this));
}

PaymentController.prototype.processStripeDone = function(data, textStatus, jqXHR) {
  if (data['errors'].length > 0) {
    var errorMsgs = new Array();
    data['errors'].forEach(function(error) {
      errorMsgs.push(error);
    });
    this.closeLoading();
    this.showPaymentError(errorMsgs);
  } else {
    window.location.href = '/thank-you';
  }
}

PaymentController.prototype.processStripeFailed = function(jqXhr, textStatus, errorThrown) {
  var errorMessages = [];
  if (typeof jqXhr.responseJSON == 'undefined') {
    errorMessages = [t('t-payment-processor-failed') + textStatus + ": " + errorThrown];
  } else {
    errorMessages = jqXhr.responseJSON['errors'];
  }
  this.showPaymentError(errorMessages);
  this.closeLoading();
}

PaymentController.prototype.isValidEmail = function(email) {
  if (email.trim() == '') {
    return false;
  }
  return (email.includes('@') && !email.includes(','));
}

PaymentController.prototype.requestCampaignTotals = function() {
  var ajaxSettings = {
    contentType: 'application/json; charset=UTF-8',
    dataType: 'json',
    url: '/campaign-totals',
  };
  $.ajax(ajaxSettings)
    .done($.proxy(this.getCampaignTotalsDone, this))
    .fail($.proxy(this.getCampaignTotalsFailed, this));
}

PaymentController.prototype.setCampaignTotals = function(data) {
  var totalDonated = roundTo(data['totalAmount'], 0);
  var totalMatched = totalDonated * 2;
  data['totalDonated'] = numeral(totalDonated).format('0');
  data['totalMatched'] = numeral(totalMatched).format('0');
  data['totalContributors'] = numeral(data['totalContributors']).format('0');
  data['totalDonations'] = numeral(data['totalDonations']).format('0');
  this.totalDonatedFlasher.resolve(data['totalDonated']);
  this.totalMatchedFlasher.resolve(data['totalMatched']);
  this.totalDonationsFlasher.resolve(data['totalDonations']);
}

PaymentController.prototype.setupCampaignTotals = function() {
  this.totalDonatedFlasher = new Flasher('.total-donated');
  this.totalMatchedFlasher = new Flasher('.total-matched');
  this.totalDonationsFlasher = new Flasher('.supporters');
  this.requestCampaignTotals();
}

PaymentController.prototype.updateDonateButtonText = function() {
  var donateSubmitAmount = $('#donate-submit-amount');
  var text = numeral(this.amount / 100).format('$0.00');
  if (this.recurring()) {
    text += ' ' + t('t-per-month');
  }
  donateSubmitAmount.html(text);
}

PaymentController.prototype.validateRequiredFields = function() {
  var requiredFields;
  var errors = [];
  try {
    requiredFields = this.getRequiredFields();
  }
  catch (e) {
    errors.push(e.message);
    $("#generic-validation-messages").text(e.message);
    return false;
  }

  // Enforce minimum donation amount of $2
  var otherAmountFields = $('#otherAmount');
  if (this.amount < 200) {
    $(".perk-desc.donate-amount-min").show();
    otherAmountFields.addClass('error');
    errors.push('$2 minimum donation.');
    $('html, body').animate({
      scrollTop: $(".perk-desc.donate-amount-min:visible").offset().top
    }, 500);
  }
  else {
    $(".perk-desc.donate-amount-min").hide();
    otherAmountFields.removeClass('error');
  }

  requiredFields.each(function(index, field) {
    var field = $(field);
    var emptyValue = '';
    if (field.is('select')) {
      emptyValue = '--none--';
    }
    var value = field.val();
    if (value == emptyValue) {
      field.removeClass('required');
      field.addClass('error');
      var fieldName = null;
      var message = null;
      if (field.is('[placeholder]')) {
        fieldName = field.attr('placeholder');
      }
      else if (field.is('[aria-label]')) {
        fieldName = field.attr('aria-label');
      }
      if (fieldName) {
        message = t('t-field-required', {field_name: fieldName});
      }
      else {
        message = t('t-field-required-generic');
      }
      errors.push(message);
    } else {
      field.removeClass('error');
      field.addClass('required');
    }
  });

  // Validate email address.
  var emailField = $('#email');
  var emailAddress = emailField.val();
  if (! this.isValidEmail(emailAddress)) {
    emailField.addClass('error');
    emailField.removeClass('required');
    errors.push(t('t-invalid-email'));
    $('html, body').animate({
      scrollTop: $("#email").offset().top
    }, 500);
  }
  else {
    emailField.removeClass('error');
    emailField.addClass('required');
  }

  return errors;
}

module.exports = {
  'PaymentController': PaymentController,
  'PaypalProcessor': require('./paypal_processor.js'),
  'GiftMatchingController': GiftMatchingController,
};
