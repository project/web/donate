<?php

namespace Tor;

class AccessControlMiddleware {
  const ONION_HOST_MAP = [
//    'rjrsgw3y2wzqmnvv.onion' => 'https://gsxohj375bk7gjal.onion', # prod
    'qbnprwaqyglijwqq.onion' => 'https://y7pm6of53hzeb7u2.onion', # stag
    'g2xie2z5bp5f6doi.onion' => 'https://y7pm6of53hzeb7u2.onion', # test
    'wiofesr5qt2k7qrlljpk53isgedxi6ddw6z3o7iay2l7ne3ziyagxaid.onion' => 'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion', #new prod
    '7niqsyixinnhxvh33zh5dqnplxnc2yd6ktvats3zmtbbpzcphpbsa6qd.onion' => 'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion',
    'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion' => 'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion',
    'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion',
    'rjrsgw3y2wzqmnvv.onion' => 'https://yoaenchicimox2qdc47p36zm3cuclq7s7qxx6kvxqaxjodigfifljqqd.onion',

];

  public function __invoke($request, $response, $next) {
    $host = @$request->getHeader('Origin')[0];
    $allowOriginUrl = $this->torSiteBaseUrl;
//used for testing
//$allowOriginUrl = 'https://kez.pages.torproject.net';
    if ($host and array_key_exists($host, static::ONION_HOST_MAP)) {
    $allowOriginUrl = static::ONION_HOST_MAP[$host];
// used for testing
//$allowOriginUrl = 'https://kez.pages.torproject.net';
    }
    $response = $response->withHeader('Access-Control-Allow-Origin', $allowOriginUrl);
    $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    return $next($request, $response);
  }

  function __construct($app) {
    $this->app = $app;
    $this->container = $app->getContainer();
    $this->torSiteBaseUrl = $this->container->get('settings')['torSiteBaseUrl'];
  }
}
