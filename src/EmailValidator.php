<?php

namespace Tor;

class EmailValidator {
  public static function validate($emailAddress, $friendlyName) {
    $errors = PresenceValidator::validate($emailAddress, $friendlyName);
    if (!empty($errors)) {
      return $errors;
    }
    $atPos = strpos($emailAddress, '@');
    $atCount = substr_count($emailAddress, '@');
    if ($atPos === FALSE) {
      $errors[] = "$friendlyName must contain an at symbol (@).";
    } elseif ($atCount > 1) {
      $errors[] = "$friendlyName connot contain more than one at symbol (@).";
    }
    elseif ($atPos == strlen($emailAddress) - 1) {
      $errors[] = "$friendlyName cannot end with an at symbol (@).";
    } elseif ($atPos == 0) {
      $errors[] = "$friendlyName cannot start with an at symbol (@).";
    }
    if (strpos($emailAddress, ',') !== FALSE) {
      $errors[] = "$friendlyName cannot contain a comma (,).";
    }
    return $errors;
  }
}
