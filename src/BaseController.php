<?php

namespace Tor;

class BaseController {
  public $container;
  public $environment_info;
  public $logger;
  public $logger_ratelimit;
  public $renderer;
  public $router;
  protected $hmac;
  protected $ratelimit;

  public function __construct($container, $language = NULL) {
    $this->container = $container;
    $this->environment_info = $container->get('environment_info');
    $this->logger = $container->get('logger');
    $this->logger_ratelimit = $container->get('logger_ratelimit');
    $this->renderer = $container->get('renderer');
    $this->router = $container->get('router');
    $this->hmac = $container->get('hmac');
    $this->ratelimit = $container->get('ratelimit');
  }
}
