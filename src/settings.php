<?php

define('PP_CONFIG_PATH', __DIR__ . '/../private');
require PP_CONFIG_PATH . '/settings.local.php';

if (!isset($localOptions)) {
  $localOptions = [];
}

if (!array_key_exists('torSiteBaseUrl', $localOptions)) {
  $localOptions['torSiteBaseUrl'] = 'https://www.torproject.org/';
}

$config = [
  'settings' => [
    'secret_key' => $secret_key,
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false,
    'ipRateLimiter' => [
      'maxRequestsPerTimeSpan' => 10,
      'timeSpan' => 3600,
    ],
    'emailRateLimiter' => [
      'maxRequestsPerTimeSpan' => 10,
      'timeSpan' => 3600,
    ],
    'renderer' => [
      'cache_path' => __DIR__ . '/../tmp/cache/templates',
      'template_path' => __DIR__ . '/../templates/',
    ],
    'logger' => [
      'name' => 'slim-app',
      'path' => __DIR__ . '/../logs/app.log',
    ],
    'logger_ratelimit' => [
      'name' => 'donate-ratelimit',
      'path' => __DIR__ . '/../logs/ratelimit.log',
    ],
    'language_default' => 'en',
    'languages' => [
      'de' => 'de_DE',
      'de_DE' => 'de_DE',
      'el' => 'el_GR',
      'el_GR' => 'el_GR',
      'en' => 'en_US',
      'en_US' => 'en_US',
      'en_UK' => 'en_UK',
      'es' => 'es_ES',
      'es_AR' => 'es_AR',
      'es_ES' => 'es_ES',
      'fr' => 'fr_FR',
      'fr_FR' => 'fr_FR',
      'is' => 'is_IS',
      'is_IS' => 'is_IS',
      'it' => 'it_IT',
      'it_IT' => 'it_IT',
      'ka' => 'ka_GE',
      'ka_GE' => 'ka_GE',
      'pt' => 'pt_BR',
      'pt_BR' => 'pt_BR',
      'pt_PT' => 'pt_PT',
      'tr' => 'tr_TR',
      'tr_TR' => 'tr_TR',
      'zh' => 'zh_CN',
      'zh_CN' => 'zh_CN',
    ],
    'paypalClientId' =>  [
      'sandbox' => 'ATRgGpitwhmV5P4QsvtBhp1O-MFaC5iiHpw8QX0nWoZhMQIswLGf2ziOxGQ8Dz49zZo6dDqzqteH8e9D',
      'production' => 'AbzwWJgUgwGMvTupfvigx1yAj75jNnCj4mFGC_VbiROUi2TfVwPS9IZ5MfqLsSUnXJFSMwaSaTR4FY_A'
    ],
    'ratelimit' => [
      'window_seconds' => 10,
      'request_limit' => 3,
    ],
  ],
];

$config['settings'] = array_merge($config['settings'], $localOptions);

return $config;
