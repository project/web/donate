<?php

namespace Tor;

class CivicrmMailingController extends BaseController {
  public static $validMethods = [
    'optout',
    'resubscribe',
    'unsubscribe',
  ];

  public function process($request, $response, $args) {
    $method = $args['method'];
    if (!in_array($method, static::$validMethods)) {
      throw new \Slim\Exception\NotFoundException($request, $response);
    }
    $queryParams = $request->getQueryParams();
    $messageInfo = array(
      'hash' => ArrayExt::fetch($queryParams, 'h'),
      'job_id' => ArrayExt::fetch($queryParams, 'jid'),
      'method' => $method,
      'queue_id' => ArrayExt::fetch($queryParams, 'qid'),
    );
    $errors = array();
    foreach ($messageInfo as $key => $value) {
      if ($value === NULL || trim($value) == "") {
        $errors[] = "Missing $key parameter.";
      }
    }
    if (empty($errors)) {
      $crmController = new CrmController($this->container);
      $crmController->sendMessage('Tor\Mailing\SubscriptionChange', $messageInfo);
      $this->renderer->render($response, "{$method}.twig");
    } else {
      $subscriptionErrorUrl = $this->container->get('settings')->get('civiMailingErrorUrl');
      if ($subscriptionErrorUrl === NULL) {
        $this->vars = array(
          'errors' => $errors,
          'langcode' => $request->getAttribute('language')
        );
        $this->renderer->render($response, 'error.twig', $this->vars);
      } else {
        $uri = Uri::createFromString($subscriptionErrorUrl);
        $uri->addQueryParam('errors', json_encode($errors));
        return $response->withRedirect($uri->toString());
      }
    }
  }
}
