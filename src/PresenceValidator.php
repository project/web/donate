<?php

namespace Tor;

class PresenceValidator {
  static function validate($value, $friendlyName) {
    $value = trim($value);
    if ($value == '') {
      return array("You must enter something for $friendlyName.");
    }
    return array();
  }
}
