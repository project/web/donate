<?php

namespace Tor;

class SubscriptionController extends BaseController {
  const REDIS_PREFIX = "subscription_request:";
  public $vars;
  public $redisClient = NULL;
  public $settings;

  public function __construct($container) {
    parent::__construct($container);
    $this->vars = array();
    $this->settings = $this->container->get('settings');
  }

  public function getRedisClient() {
    if ($this->redisClient == NULL) {
      $this->redisClient = new \Predis\Client($this->settings['redis']);
    }
    return $this->redisClient;
  }

  public function getTokenKey($token) {
    return static::REDIS_PREFIX . $token;
  }

  public function form($request, $response, $args) {
    $this->vars['bodyClasses'] = array('subscribe');
    $this->vars['headerHasBgImg'] = TRUE;
    return $this->renderer->render($response, 'subscribe.twig', $this->vars);
  }

  public function subscriptionConfirm($request, $response, $args) {
    $errors = array();
    $queryParams = $request->getQueryParams();
    $token = ArrayExt::fetch($queryParams, 'token');
    if ($token === NULL) {
      $errors[] = "There should have been a query parameter called token in the URL we sent you, but that seems to be missing. If you cut and pasted the URL, double check that you got the entire string.";
    }
    if (empty($errors)) {
      $redis = $this->getRedisClient();
      $key = $this->getTokenKey($token);
      $data = $redis->get($key);
      if ($data === NULL) {
        $errors[] = "We couldn't find the token for this subscription confirmation in our database. Please fill out the form below to get the confirmation email resent.";
      }
    }
    if (empty($errors)) {
      $subscriptionInfo = unserialize($data);
      if ($subscriptionInfo === FALSE) {
        throw new \Exception("Error unserializing '" . print_r($data, TRUE) . " for subscription confirmation.");
      }
      $crmController = new CrmController($this->container);
      $crmController->sendMessage('Tor\Subscription\Subscribe', $subscriptionInfo);
      $counter = new SubscriptionCounter($this->container);
      $counter->countSubscriptionConfirmed();
      return $response->withRedirect($this->settings->get('subscriptionConfirmedUrl', '/subscribed'));
    } else {
      $subscriptionErrorUrl = $this->settings->get('subscriptionErrorUrl');
      if ($subscriptionErrorUrl === NULL) {
        $this->vars['errors'] = $errors;
        return $this->renderer->render($response, 'subscribe.twig', $this->vars);
      } else {
        $uri = Uri::createFromString($subscriptionErrorUrl);
        $uri->addQueryParam('errors', json_encode($errors));
        return $response->withRedirect($uri->toString());
      }
    }
  }

  public function subscriptionRequest($request, $response, $args) {
    $this->vars['bodyClasses'] = array('subscribe');
    $parsedBody = $request->getParsedBody();
    $fieldValidationRules = array(
      'email' => array('EmailValidator', 'Email'),
      'firstName' => NULL,
      'lastName' => NULL,
    );
    $errors = array();
    $captcha = new \Tor\Captcha();
    if (!$captcha->is_valid($parsedBody, $_SESSION)) {
      $errors[] = "Captcha is incorrect, please check your input.";
    }
    $subscriptionInfo = array();
    foreach ($fieldValidationRules as $fieldName => $validationRule) {
      $fieldValue = ArrayExt::fetch($parsedBody, $fieldName, '');
      if (trim($fieldValue) != '') {
        $subscriptionInfo[$fieldName] = $fieldValue;
      }
      if ($validationRule !== NULL) {
        $validationFunction = array("\\Tor\\{$validationRule[0]}", 'validate');
        $fieldFriendlyName = $validationRule[1];
        $fieldErrors = call_user_func($validationFunction, $fieldValue, $fieldFriendlyName);
        $errors = array_merge($errors, $fieldErrors);
      }
    }
    if (empty($errors)) {
      $this->container->get('ipRateLimiter')->check($request);
      $this->container->get('emailRateLimiter')->check($request);
      $this->sendSubscriptionConfirmation($request, $subscriptionInfo);
      return $response->withRedirect($this->settings->get('subscriptionRequestSentUrl', '/subscription-request-sent'));
    } else {
      if (ArrayExt::fetch($parsedBody, 'returnToReferrer')) {
        $url = $this->settings->get('subscriptionErrorUrl');
        $uri = Uri::createFromString($url);
        $uri->addQueryParam('errors', json_encode($errors));
        return $response->withRedirect($uri->toString());
      }
      $this->vars['errors'] = $errors;
      $this->vars['parsedBody'] = $parsedBody;
      return $this->renderer->render($response, 'subscribe.twig', $this->vars);
    }
  }

  public function subscriptionRequestSent($request, $response, $args) {
    $this->renderer->render($response, 'subscription-request-sent.twig');
  }

  public function sendSubscriptionConfirmation($request, $subscriptionInfo) {
    $factory = new \RandomLib\Factory();
    $generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::LOW));
    $token = $generator->generateString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $redis = $this->getRedisClient();
    $data = serialize($subscriptionInfo);
    $key = $this->getTokenKey($token);
    $redis->set($key, $data);
    $redis->expire($key, 3600);
    $email = new \PHPMailer();
    \PHPMailer::$validator = 'noregex';
    $email->setFrom('newsletter@torproject.org', "The Tor Project");
    // Set the Return-Path so that bounces are processed by CiviCRM
    $email->Sender = 'civicrm@crm.torproject.org';
    $names = array(ArrayExt::fetch($subscriptionInfo, 'firstName', ''), ArrayExt::fetch($subscriptionInfo, 'lastName', ''));
    $nameString = implode(' ', $names);
    $email->addAddress($subscriptionInfo['email'], $nameString);
    $email->isHTML(TRUE);
    $email->Subject = "Please confirm your subscription to The Tor Project newsletter.";
    $router = $this->container->get('router');
    $path = $router->pathFor('subscription-confirm', array(), array('token' => $token));
    $confirmationUrl = $request->getUri()->getBaseUrl() . $path;
    $twigVariables = $subscriptionInfo;
    $twigVariables['host'] = $request->getHeaderLine('host');
    $twigVariables['confirmationUrl'] = $confirmationUrl;
    $email->Body = $this->renderer->fetch("emails/subscription-confirmation.html.twig", $twigVariables);
    $email->AltBody = $this->renderer->fetch("emails/subscription-confirmation.txt.twig", $twigVariables);
    if (!$email->send()) {
      throw new \Exception("Error sending subscription confirmation email to {$subscriptionInfo['email']} " . $mail->ErrorInfo);
    }
    $counter = new SubscriptionCounter($this->container);
    $counter->countSubscriptionRequest();
  }

  public function subscribed($request, $response, $args) {
    $this->vars['bodyClasses'] = array('subscribed');
    return $this->renderer->render($response, 'subscribed.twig', $this->vars);
  }
}
