<?php

namespace Tor;

class EmailRateLimiter {
  public $redis;
  public $environment_info;

  public function __construct($container) {
    $this->environment_info = $container->get('environment_info');
    $this->logger = $container->get('logger');
    $this->redis = \Resque::redis();
    $this->settings = $container->get('settings')['emailRateLimiter'];
    $this->maxRequestsPerTimeSpan = $this->settings['maxRequestsPerTimeSpan'];
    $this->timeSpan = $this->settings['timeSpan'];
  }

  function check($request) {
    $keyName = $this->keyName($request);
    list($allowance, $lastCheck) = $this->getLimits($keyName);
    $now = time();
    $timePassed = $now - $lastCheck;
    $allowanceAdjustment = $timePassed * $this->maxRequestsPerTimeSpan / $this->timeSpan;
    $allowance += $allowanceAdjustment;
    if ($allowance < 1) {
      $this->setLimits($keyName, $allowance, $now);
      $ipAddress = $request->getAttribute('ip_address');
      $parsedBody = $request->getParsedBody();
      if (array_key_exists('fields', $parsedBody)) {
          $email = ArrayExt::fetch($parsedBody['fields'], 'email', '');
      } else {
          $email = ArrayExt::fetch($parsedBody, 'email', '');
      }
      $keyname = $this->keyName($request);
      throw new EmailRateExceeded("Blocked request: rate limit exceeded from email $email (IP address $ipAddress): {$this->maxRequestsPerTimeSpan} requests in {$this->timeSpan} seconds.");
    } elseif ($allowance > $this->maxRequestsPerTimeSpan) {
      $allowance = $this->maxRequestsPerTimeSpan;
    }
    $allowance -= 1;
    $this->setLimits($keyName, $allowance, $now);
  }

  function getLimits($keyName) {
    $data = $this->redis->get($keyName);
    if (is_null($data)) {
      return [$this->maxRequestsPerTimeSpan, time()];
    }
    $struct = unserialize($data, ['allowed_classes', FALSE]);
    if ($struct === FALSE) {
      return [$this->maxRequestsPerTimeSpan, time()];
    }
    return unserialize($data);
  }

  function keyName($request) {
    $parsedBody = $request->getParsedBody();
    if (array_key_exists('fields', $parsedBody)) {
      $key = rawurlencode(ArrayExt::fetch($parsedBody['fields'], 'email', ''));
    } else {
      $key = rawurlencode(ArrayExt::fetch($parsedBody, 'email', ''));
    }
    return $this->environment_info->name() . "_rate_limiter_$key";
  }

  function setLimits($keyName, $allowance, $lastCheck) {
    $data = serialize([$allowance, $lastCheck]);
    $this->redis->set($keyName, $data);
    $this->redis->expire($keyName, $this->timeSpan);
  }
}
