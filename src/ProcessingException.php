<?php

namespace Tor;

class ProcessingException extends \Exception {
  public $paypalResponse;

  public function __construct($paypalResponse) {
    $this->paypalResponse = $paypalResponse;
    parent::__construct($this->combinedLongMessages());
  }

  public function combinedLongMessages() {
    return implode("\n", $this->longMessages());
  }

  public function longMessages() {
    $messages = array();
    foreach ($this->paypalResponse->Errors as $error) {
      $messages[] = $error->LongMessage;
    }
    return $messages;
  }
}
