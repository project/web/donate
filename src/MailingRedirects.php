<?php

namespace Tor;

class MailingRedirects {
  public static $redirectLookup = array(
    '608' => 'https://torproject.org/donate/donate-an-sin-em',
    '609' => 'https://torproject.org/donate/donate-an-sin-em',
    '610' => 'https://facebook.com/torproject',
    '611' => 'http://twitter.com/torproject',
    '612' => 'https://instagram.com/torproject',
    '613' => 'https://torproject.org',
  );
  function process($request, $response, $args) {
    $queryParams = $request->getQueryParams();
    $u = ArrayExt::fetch($queryParams, 'u');
    if ($u !== NULL) {
      $targetUrl = ArrayExt::fetch(static::$redirectLookup, $u);
      if ($targetUrl !== NULL) {
        return $response->withRedirect($targetUrl);
      }
    }
    return $response->withRedirect("/");
  }
}
