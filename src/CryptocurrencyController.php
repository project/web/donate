<?php

namespace Tor;

use Tor\CampaignController;

class CryptocurrencyController extends BaseController {
  public static $REQUIRED_FIELDS = array(
    'cryptocurrencyType',
    'currencyAmount',
    'email',
    'estimatedDonationDate',
  );

  public static $WALLETS = [[
    'symbol' => 'REP',
    'id' => '0x19DdD94B94D3c68385c897846AB44Ac99DBFAe0f',
    'name' => 'Augur',
  ], [
    'symbol' => 'XBT',
    'id' => '1JADsmDFX9d2TXis63S9F9L8eDAXwJmnWE',
    'name' => 'Bitcoin',
  ], [
    'symbol' => 'BCH',
    'id' => '1BK6d6vfLPDqZaBeKRd15oK5yxuLymi1Ry',
    'name' => 'Bitcoin Cash',
  ], [
    'symbol' => 'DASH',
    'id' => 'Xkknor7eBf6vADMtP8VuNTPKsTsatCp8Xe',
    'name' => 'Dash',
  ], [
    'symbol' => 'ETH',
    'id' => '0x19DdD94B94D3c68385c897846AB44Ac99DBFAe0f',
    'name' => 'Ether',
  ], [
    'symbol' => 'LTC',
    'id' => 'LNAifc8nfjtDJ8azRPiancbZSBftPzhfzb',
    'name' => 'Litecoin',
  ], [
    'symbol' => 'XMR',
    'id' => '',
    'name' => 'Monero',
  ], [
    'symbol' => 'XLM',
    'id' => 'GABWGQEQESRX5TKDTPIYJFPKGJDMEW6VLOOLBTIFPJIN7XT6KAFXJQPJ',
    'name' => 'Stellar Lumen',
  ], [
    'symbol' => 'ZEC',
    'id' => 'zs1kpat3qseujnnukms9yjkx7w3kgzev7jxhauc6cy2s3mupmvsvkvw04u3s35sffmv57leznctn5h',
    'name' => 'Zcash',
  ], [
    'symbol' => 'XDG',
    'id' => 'DGvn1HLeMaCZEZZYUeBWBhUCJiS2hjzbGd',
    'name' => 'Dogecoin',
  ],
];

  public function index($request, $response, $args) {
    $vars = array(
      'bodyClasses' => ['cryptocurrency', 'cryptocurrency-main', 'title-header-image'],
      'wallets' => static::$WALLETS,
      'wallets_json' => json_encode(static::$WALLETS),
      'isMatchingDonation' => CampaignController::is_matching_donation($now),
    );
    return $this->renderer->render($response, 'cryptocurrency.twig', $vars);
  }

  public function donate($request, $response, $args) {
    $parsedBody = $request->getParsedBody();
    $missingFieldNames = $this->validate($parsedBody);
    if (!empty($missingFieldNames)) {
      $error = "Missing required fields: " . implode(", ", $missingFieldNames);
      $responseData = array(
          'errors' => array($error),
      );
      $this->logger->error($error . ", from: " . $_SERVER['REMOTE_ADDR']);
      return $response->withJson($responseData);
    }
    $fieldHelper = new FieldHelper();
    $contact = $fieldHelper->createFieldArray($parsedBody, FieldHelper::$contactFieldNames);
    $shippingAddress = $fieldHelper->createFieldArray($parsedBody, FieldHelper::$shippingFieldNames);
    if (!empty($shippingAddress)) {
      $contact['shippingAddress'] = $shippingAddress;
    }
    $donationInfo = array(
      'contact' => $contact,
      'cryptocurrency_amount' => ArrayExt::fetch($parsedBody, 'currencyAmount'),
      'crytpocurrency_type' => ArrayExt::fetch($parsedBody, 'cryptocurrencyType'),
      'estimated_donation_date' => ArrayExt::fetch($parsedBody, 'estimatedDonationDate'),
      'trxn_id' => $this->generateTrxnId(),
    );
    $crmController = new CrmController($this->container);
    $crmController->sendMessage('Tor\Donation\CryptoIntention', $donationInfo);

    $cryptocurrencyThankyouUrl = $this->container->get('settings')->get('cryptocurrencyThankyouUrl');
    if ($cryptocurrencyThankyouUrl === NULL) {
      $cryptocurrencyThankyouUrl = '/cryptocurrency/thank-you';
    }
    return $response->withRedirect($cryptocurrencyThankyouUrl);
  }

  public function generateTrxnId() {
    return 'crypto-' . microtime(TRUE);
  }

  public function thank_you($request, $response, $args) {
    $vars = array(
      'bodyClasses' => ['cryptocurrency', 'cryptocurrency-thank-you'],
      'wallets' => static::$WALLETS,
    );
    return $this->renderer->render($response, 'cryptocurrency-thank-you.twig', $vars);
  }

  public function validate($fields) {
    $fieldHelper = new FieldHelper();
    return $fieldHelper->checkRequired($fields, static::$REQUIRED_FIELDS);
  }
}
