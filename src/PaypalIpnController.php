<?php

namespace Tor;

use GuzzleHttp\Client;

class PaypalIpnController extends BaseController {
  public function argsForOneTimeContribution($parsedBody) {
    return array(
      'contact' => array(
        'email' => ArrayExt::fetchRequired($parsedBody, 'payer_email'),
        'firstName' => ArrayExt::fetchRequired($parsedBody, 'first_name'),
        'lastName' => ArrayExt::fetchRequired($parsedBody, 'last_name'),
      ),
      'currency' => ArrayExt::fetchRequired($parsedBody, 'mc_currency'),
      'misc' => array(
        'comments' => ArrayExt::fetch($parsedBody, 'memo'),
      ),
      'payment_instrument_id' => 'PayPal',
      'perk' => NULL,
      'receive_date' => ArrayExt::fetchRequired($parsedBody, 'payment_date'),
      'total_amount' => ArrayExt::fetchRequired($parsedBody, 'mc_gross'),
      'trxn_id' => ArrayExt::fetchRequired($parsedBody, 'txn_id'),
    );
  }

  public function argsFromIpnMessage($parsedBody, $statusId) {
    return array(
      'currency' => ArrayExt::fetchRequired($parsedBody, 'mc_currency'),
      'payment_instrument_id' => 'PayPal',
      'receive_date' => ArrayExt::fetchRequired($parsedBody, 'payment_date'),
      'total_amount' => ArrayExt::fetchRequired($parsedBody, 'mc_gross'),
      'trxn_id' => ArrayExt::fetchRequired($parsedBody, 'txn_id'),
      'contribution_status_id' => $statusId,
    );
  }

  public function index($request, $response, $args) {
    if (!$this->environment_info->is_dev()) {
      $this->verify_request($request);
    }
    $parsedBody = $request->getParsedBody();
    $now = new \DateTime();
    $logPath = __DIR__ . "/../logs/paypal.log";
    $file = fopen($logPath, 'a');
    fwrite($file, $now->format(\DateTime::ATOM) . ": " . print_r($parsedBody, TRUE));
    fclose($file);
    $transactionType = ArrayExt::fetch($parsedBody, 'txn_type');
    $methodName = "process_$transactionType";
    if (method_exists($this, $methodName)) {
      $crmController = new CrmController($this->container);
      $this->$methodName($crmController, $parsedBody);
    }
  }

  public function ipn_verification_url() {
    if ($this->environment_info->is_prod()) {
      return 'https://ipnpb.paypal.com/cgi-bin/webscr';
    } else {
      return 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
    }
  }

  public function process_recurring_payment($crmController, $parsedBody) {
    $args = $this->argsFromIpnMessage($parsedBody, 'Completed');
    $args['recurring_contribution_transaction_id'] = ArrayExt::fetchRequired($parsedBody, 'recurring_payment_id');
    $crmController->sendMessage('Tor\Donation\RecurringContributionOngoing', $args);
  }

  public function process_recurring_payment_failed($crmController, $parsedBody) {
    $args = array(
      'contribution_status_id' => 'Failed',
      'recurring_contribution_transaction_id' => ArrayExt::fetchRequired($parsedBody, 'recurring_payment_id'),
    );
    $crmController->sendMessage('Tor\Donation\RecurringContributionFailed', $args);
  }

  public function process_recurring_payment_profile_cancel($crmController, $parsedBody) {
    $args = array(
      'recurring_contribution_transaction_id' => ArrayExt::fetchRequired($parsedBody, 'recurring_payment_id'),
    );
    $crmController->sendMessage('Tor\Donation\RecurringContributionCancel', $args);
  }

  public function process_send_money($crmController, $parsedBody) {
    $args = $this->argsForOneTimeContribution($parsedBody);
    $args['source'] = 'PayPal Send Money Service';
    $crmController->sendMessage('Tor\Donation\OneTimeContribution', $args);
  }

  public function process_subscr_cancel($crmController, $parsedBody) {
    $args = array(
      'recurring_contribution_transaction_id' => ArrayExt::fetchRequired($parsedBody, 'subscr_id'),
    );
    $crmController->sendMessage('Tor\Donation\RecurringContributionCancel', $args);
  }

  public function process_subscr_failed($crmController, $parsedBody) {
    $args = array(
      'contribution_status_id' => 'Failed',
      'recurring_contribution_transaction_id' => ArrayExt::fetchRequired($parsedBody, 'subscr_id'),
    );
    $crmController->sendMessage('Tor\Donation\RecurringContributionFailed', $args);
  }

  public function process_subscr_payment($crmController, $parsedBody) {
    $paymentStatus = ArrayExt::fetch($parsedBody, 'payment_status');
    if ($paymentStatus == NULL || $paymentStatus != 'Completed') {
      return;
    }
    $args = $this->argsFromIpnMessage($parsedBody, $paymentStatus);
    $args['recurring_contribution_transaction_id'] = ArrayExt::fetchRequired($parsedBody, 'subscr_id');
    $crmController->sendMessage('Tor\Donation\RecurringContributionOngoing', $args);
  }

  public function process_web_accept($crmController, $parsedBody) {
    $args = $this->argsForOneTimeContribution($parsedBody);
    $args['source'] = 'PayPal Button';
    $crmController->sendMessage('Tor\Donation\OneTimeContribution', $args);
  }

  public function verify_request($request) {
    $body = $request->getBody()->getContents();
    $body = $body . "&cmd=_notify-validate";
    $client = new Client();
    $options = array(
      'body' => $body,
      'headers' => array(
        'User-Agent' => 'Tor Project PayPal IPN Validator',
      ),
    );
    $response = $client->request('POST', $this->ipn_verification_url(), $options);
    $responseBody = $response->getBody()->getContents();
    if ($responseBody != 'VERIFIED') {
      throw new \Exception("PayPal didn't verify the request:\n$body\nResponse:\n$responseBody");
    }
  }
}
