<?php

namespace Tor;

class FieldHelper {
  public static $customerFieldNames = array(
    'email',
    'firstName',
    'lastName',
  );
  public static $contactFieldNames = array(
    'email',
    'firstName',
    'lastName',
    'mailingListOptIn',
  );
  public static $shippingFieldNames = array(
    'country',
    'countryName',
    'extendedAddress',
    'locality',
    'postalCode',
    'region',
    'streetAddress',
  );
  public static $miscFieldNames = array(
    'comments',
  );

  public function checkRequired($fields, $requiredFieldNames) {
    $missingFieldNames = array();
    foreach ($requiredFieldNames as $requiredFieldName) {
      $value = ArrayExt::fetch($fields, $requiredFieldName);
      if ($this->isBlank($value)) {
        $missingFieldNames[] = $requiredFieldName;
      }
    }
    return $missingFieldNames;
  }

  public function createFieldArray($source, $fieldNames) {
    $target = array();
    foreach ($fieldNames as $fieldName) {
      $value = ArrayExt::fetch($source, $fieldName);
      if (!$this->isBlank($value)) {
        $target[$fieldName] = $value;
      }
    }
    return $target;
  }

  public function createDonationInfo($parsedBody) {
    $fields = ArrayExt::fetch($parsedBody, 'fields');
    $perk = ArrayExt::fetch($parsedBody, 'perk');
    $contact = $this->createFieldArray($fields, static::$contactFieldNames);
    $contact['shippingAddress'] = $this->createFieldArray($fields, static::$shippingFieldNames);
    $misc = $this->createFieldArray($fields, static::$miscFieldNames);
    $donationInfo = array(
      'contact' => $contact,
      'perk' => $perk,
      'misc' => $misc,
    );
    return $donationInfo;
  }

  public function isBlank($value) {
    if ($value === NULL) {
      return TRUE;
    }
    if (trim($value) == '') {
      return TRUE;
    }
    return FALSE;
  }
}
