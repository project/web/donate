<?php

namespace Tests\Unit;

class WebhookControllerTest extends \PHPUnit_Framework_TestCase {
  public function setUp() {
    $this->container = $this->createMock(\stdClass::class);
    $this->container->logger = new \Monolog\Logger('test');
    $this->log_handler = new \Monolog\Handler\TestHandler();
    $this->container->logger->pushHandler($this->log_handler);
    $environment = $this->getMockBuilder(stdClass::class);
    $environment->setMethods(array('get'));
    $environment = $environment->getMock();
    $environment->expects($this->any())->method('get')->with('APP_ENV', 'dev')->willReturn('test');
    $this->container->environment = $environment;
    $this->container->environment_info = new \Tor\EnvironmentInfo($this->container);
  }

  public function test_index_calls_handle_check() {
    $controller = new \Tor\WebhookController($this->container);
    $request = new MockBraintreeRequest('check', 'foo');
    $controller->index($request, NULL, NULL);
    $this->assertTrue($this->log_handler->hasRecordThatContains('Got check notification.', \Monolog\Logger::DEBUG));
  }

  public function test_unhandled_notification_throws_exception() {
    $this->expectException(\Exception::class);
    $request = new MockBraintreeRequest(\Braintree\WebhookNotification::PARTNER_MERCHANT_CONNECTED);
    $controller = new \Tor\WebhookController($this->container);
    $controller->index($request, NULL, NULL);
  }

  public function test_subscription_cancelled() {
    $request = new MockBraintreeRequest(\Braintree\WebhookNotification::SUBSCRIPTION_CANCELED);
    $controller = new \Tor\WebhookController($this->container);
    $controller->index($request, NULL, NULL);
  }
}
