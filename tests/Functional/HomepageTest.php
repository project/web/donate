<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase {
    public function testGetHomepageWithoutName() {
        $response = $this->runApp('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPostHomepageNotAllowed() {
        $response = $this->runApp('POST', '/', ['test']);
        $this->assertEquals(405, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string)$response->getBody());
    }
}
