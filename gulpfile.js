var gulp = require('gulp');
var exec = require('child_process').exec;
var path = require('path');
var gutil = require('gulp-util');
var walk = require('walk');
var localeConfig = require('./translation/locale.json');
var mkdirp = require('mkdirp');

function execOutput(error, stdout, stderr, msg) {
  if (msg != "") {
    gutil.log(msg);
  }
  if (error) {
    new gutil.PluginError({
      plugin: "tor-local",
      message: `exec error: ${error}`
    });
  }
  if (stdout != "") {
    gutil.log(`stdout: ${stdout}`);
  }
  if (stderr != "") {
    gutil.log(`stderr: ${stderr}`);
  }
}

gulp.task('locale-rebuild-code-cache', function(cb){
  exec('php i18n.php', function(error, stdout, stderr) {
    execOutput(error, stdout, stderr, 'Rebuilding code cache');
    cb();
  });
});

gulp.task('locale-xgettext', ['locale-rebuild-code-cache'],  function(cb){
  var codeFiles = [];
  walker = walk.walk('tmp/cache_locale');
  walker.on('file', function (root, fileStats, next) {
    if (fileStats.type == 'file') {
      codeFiles.push(`${root}/${fileStats.name}`);
    }
    next();
  });
  walker.on('error', function(root, nodeStatsArray, next) {
    new gutil.PluginError({
     plugin: "tor-local",
      message: 'Error walking code files during locale-index'
    });
    next();
  });
  walker.on('end', function() {
    var allFiles = codeFiles.join(' ');
    exec(`xgettext -o translation/out/messages.pot --add-comments=notes --from-code=UTF-8 -n --omit-header ${allFiles}`, function(error, stdout, stderr) {
      execOutput(error, stdout, stderr, 'Running xgettext on code cache');
      cb();
    });
  });
});

// Compile mo files from po files.
gulp.task('locale-msgfmt', function(){
  localeConfig.locales.forEach(function(locale) {
    var localeInput = `translation/in/locale/${locale}/LC_MESSAGES/messages.po`;
    var localeOutputDir = `translation/built/locale/${locale}/LC_MESSAGES`;
    mkdirp.sync(localeOutputDir);
    var localeOutput = `${localeOutputDir}/messages.mo`;
    var command = `msgfmt -c -o ${localeOutput} ${localeInput}`;
    exec(`msgfmt -c -o ${localeOutput} ${localeInput}`, function(error, stdout, stderr) {
      execOutput(error, stdout, stderr, `Running msgfmt for ${locale}.`);
    });
  });
});

// Alias for locale-xgettext.
gulp.task('locale-compile-mo', ['locale-msgfmt']);
